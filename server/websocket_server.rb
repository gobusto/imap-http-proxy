# frozen_string_literal: true

require 'em-websocket'
require_relative 'app'

# We're not using a standard Rack web server, so we do this bit explicitly:
HOST = ENV['HOST'] || '0.0.0.0'
PORT = ENV['PORT'] || 3000
puts("Starting a websocket server on #{HOST}:#{PORT}")

app = App.new
EM.run do
  EM::WebSocket.run(host: HOST, port: PORT) do |socket|
    socket.onopen { app.connect(socket.object_id) }
    socket.onclose { app.disconnect(socket.object_id) }

    socket.onmessage do |data|
      result = app.receive(socket.object_id, data)
      socket.send(result.dup) if result
    rescue StandardError => e
      puts("[ERROR] #{e}\n#{e.backtrace}")
    end
  end
end
