# frozen_string_literal: true

require 'date'
require 'json'
require_relative 'emails/reader'

# This class essentially serves as a "manager" for websocket connections.
class App
  KNOWN_ACTIONS = %w[authenticate list fetch].freeze

  def initialize
    @session = {}
  end

  def connect(cid)
    puts("[#{cid}] Connected")
    @session[cid] ||= { reader: Emails::Reader.new(ENV['IMAP_SERVER']) }
  end

  def disconnect(cid)
    puts("[#{cid}] Disconnected")
    @session.delete(cid)
  end

  def receive(cid, data)
    data = JSON.parse(data)
    action = data.delete('action')
    return unknown_action(action).to_json unless KNOWN_ACTIONS.include?(action)

    { 'action' => action, 'data' => send(action, cid, data) }.to_json
  end

  private

  def authenticate(cid, data)
    reader = @session[cid][:reader]
    reader.authenticate(data['username'], data['password'])
    {} # No data.
  end

  def list(cid, data)
    attrs = data.dup
    path = attrs.delete('path')
    @session[cid][:reader].list(path, **attrs)
  end

  def fetch(cid, data)
    @session[cid][:reader].fetch(data['path'], data['seqno'])
  end

  def unknown_action(action)
    { 'error' => "Unknown action: #{action}" }
  end
end
