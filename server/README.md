IMAP-to-HTTP bridge server
==========================

A simple Ruby project to proxy HTTP (or websocket) requests to an IMAP server.

Getting started (REST API)
--------------------------

Ensure that you have the `sinatra` gem installed:

    gem install sinatra

...then start a server, optionally specifying a host (`-o`) or port (`-p`):

    IMAP_SERVER=imap.gmail.com ruby rest_api_server.rb -p 8080

You can fetch a list of emails by sending a GET request like so:

    http://localhost:8080/imap/inbox?since=01-Jan-2022

...or the body of an individual message by specifying a `seqno` parameter:

    http://localhost:8080/imap/inbox?seqno=12345

Note that this opens a new IMAP connection on each request, and doesn't support
real-time updates. If these are problems, try using a websocket server instead.

Getting started (Websockets)
----------------------------

Ensure that you have the `em-websocket` gem installed:

    gem install em-websocket

...then start a server, optionally specifying the `HOST` or `PORT` to use:

    IMAP_SERVER=imap.gmail.com PORT=8080 ruby websocket_server.rb

Once connected, you can send/receive JSON data via websocket requests:

* `{ "action": "authenticate", "username": "foo", "password": "bar" }`
* `{ "action": "list", "path": "inbox", "since": "01-Jan-2022" }`
* `{ "action": "fetch", "path": "inbox", "seqno": 12345 }`

It is not currently possible to set up real-time updates from the IMAP server.

TODO
----

Aside from making things "less basic" overall:

* Set up RSpec
* Set up Rubocop
* Set up a (pair of?) Dockerfile(s)
* Add SMTP support for sending emails as well

License
-------

Copyright (c) 2022 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
