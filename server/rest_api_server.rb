# frozen_string_literal: true

require 'sinatra'
require_relative 'emails/reader'
# require_relative 'emails/sender'

# Allow AJAX requests:
def add_default_response_headers!
  headers['Access-Control-Allow-Origin'] = '*'
  headers['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
  headers['Access-Control-Allow-Headers'] = 'Authorization'
end

# Extract the encoded username and password from the "Authorization" header:
def username_and_password
  auth = Rack::Auth::Basic::Request.new(request.env)
  (auth.provided? && auth.basic? && auth.credentials) || [nil, nil]
end

# Tell browsers to prompt the user for credentials if none are provided:
def protected!
  return unless username_and_password.any?(&:nil?)

  add_default_response_headers!
  headers['Content-Type'] = 'application/json'
  headers['WWW-Authenticate'] = 'Basic realm="Email Proxy Server"'
  halt(401, '{ "error": "Authentication required" }')
end

# Fetch data from an IMAP server and send it back to the original client:
get('/imap/*') do
  protected! # Tell browsers that they need to prompt the user for credentials.

  attrs = params.dup
  inbox = attrs.delete(:splat).join('/')
  seqno = attrs.delete(:seqno)&.to_i

  reader = Emails::Reader.new(ENV['IMAP_SERVER'])
  reader.authenticate(username_and_password.first, username_and_password.last)
  data = seqno ? reader.fetch(inbox, seqno) : reader.list(inbox, **attrs)
  reader.disconnect!

  add_default_response_headers!
  headers['Content-Type'] = 'application/json'
  data.to_json
rescue StandardError => e
  add_default_response_headers!
  headers['Content-Type'] = 'application/json'
  halt(500, { 'error' => e.to_s }.to_json)
end

# Handle "preflight" requests to allow CORS:
options('*') { add_default_response_headers! }
