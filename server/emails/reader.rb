# frozen_string_literal: true

require 'net/imap'

module Emails
  # Encapsulates the fetching of data from an IMAP server.
  class Reader
    def initialize(host, port: 993, ssl: true)
      @imap = Net::IMAP.new(host, port: port, ssl: ssl)
    end

    def authenticate(username, password)
      @imap.authenticate('PLAIN', username, password)
    end

    def list(directory, **kwargs)
      filters = kwargs.transform_keys { |key| key.to_s.upcase }.to_a.flatten

      @imap.examine(directory)
      seqnos = @imap.search(filters)
      @imap.fetch(seqnos, %w[BODY ENVELOPE]).map do |message|
        {
          'seqno' => message.seqno,
          'body' => jsonify(message.attr['BODY']),
          'envelope' => jsonify(message.attr['ENVELOPE'])
        }
      end
    end

    def fetch(directory, seqno)
      @imap.examine(directory)
      message = @imap.fetch(seqno, 'RFC822').first
      { 'seqno' => seqno, 'rfc822' => message && message.attr['RFC822'] }
    end

    def disconnect!
      @imap.disconnect
    end

    private

    def jsonify(data)
      data.to_h.map do |key, value|
        key = key.to_s.gsub(/_[a-z]/) { |s| s.slice(-1).upcase } # `.camelize`
        [key, value.is_a?(Array) ? value.map { |item| jsonify(item) } : value]
      end.to_h
    end
  end
end
